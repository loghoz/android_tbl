package com.itlabil.tambalban;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.itlabil.tambalban.auth.LoginActivity;
import com.itlabil.tambalban.menuutama.MapsActivity;
import com.itlabil.tambalban.menuutama.PengaturanActivity;
import com.itlabil.tambalban.menuutama.SoundActivity;
import com.itlabil.tambalban.menuutama.TipsActivity;
import com.itlabil.tambalban.network.ApiService;
import com.itlabil.tambalban.network.RetrofitBuilder;
import com.itlabil.tambalban.network.TokenManager;

public class MainActivity extends AppCompatActivity {

    private ImageButton btnMaps,btnTips,btnNada,btnPengaturan;

    private AdView mAdView;

//    TokenManager tokenManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //tambah admob
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

//        tokenManager = new TokenManager(this);
//
//        //jika token tidak kosong maka akan redirect ke Tambal Ban
//        if (tokenManager.getSPSudahLogin().equals(Boolean.FALSE)){
//            startActivity(new Intent(MainActivity.this, LoginActivity.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
//            finish();
//        }

        //cari id btn
        btnMaps         = (ImageButton) findViewById(R.id.btn_menu_maps);
        btnTips         = (ImageButton) findViewById(R.id.btn_menu_tips);
        btnNada         = (ImageButton) findViewById(R.id.btn_menu_musik);
        btnPengaturan   = (ImageButton) findViewById(R.id.btn_menu_pengaturan);


        btnTips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TipsActivity.class));
            }
        });

        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
            }
        });


        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PengaturanActivity.class));
            }
        });

        btnNada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SoundActivity.class));
            }
        });

    }

}
