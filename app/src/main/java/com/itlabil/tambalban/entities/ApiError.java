package com.itlabil.tambalban.entities;

import java.util.List;
import java.util.Map;

/**
 * Created by itlabil on 25/11/17.
 */

public class ApiError {
    String message;
    Map<String, List<String>> errors;

    public String getMessage() {
        return message;
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }
}
