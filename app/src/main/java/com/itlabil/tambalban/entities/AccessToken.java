package com.itlabil.tambalban.entities;

import com.squareup.moshi.Json;

/**
 * Created by itlabil on 25/11/17.
 */

public class AccessToken {
    @Json(name = "name")
    String accessName;
    @Json(name = "email")
    String accessEmail;
    @Json(name = "token")
    String accessToken;

    public String getAccessName() {
        return accessName;
    }

    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }

    public String getAccessEmail() {
        return accessEmail;
    }

    public void setAccessEmail(String accessEmail) {
        this.accessEmail = accessEmail;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
