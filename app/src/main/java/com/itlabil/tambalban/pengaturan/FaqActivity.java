package com.itlabil.tambalban.pengaturan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itlabil.tambalban.R;
import com.itlabil.tambalban.config.PengaturanListView;

public class FaqActivity extends AppCompatActivity {
    ListView faq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        final String faqTitle[] = getResources().getStringArray(R.array.faq_title);
        final String faqContent[] = getResources().getStringArray(R.array.faq_content);

        faq = (ListView)findViewById(R.id.faq);
        ArrayAdapter<String> faqAdapter = new ArrayAdapter<String>(this, R.layout.activity_faq_list_view,R.id.faqTitle, faqTitle);
        faq.setAdapter(faqAdapter);

        faq.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // set an Intent to Another Activity
                Intent intent = new Intent(FaqActivity.this, FAQViewActivity.class);
                intent.putExtra("faqTitle", faqTitle[position]); // put array faqTitle in Intent
                intent.putExtra("faqContent", faqContent[position]); // put array faqContent in Intent
                startActivity(intent); // start Intent
            }
        });
    }
}
