package com.itlabil.tambalban.pengaturan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.itlabil.tambalban.R;
import com.itlabil.tambalban.model.TambalBanModel;
import com.itlabil.tambalban.network.ApiClient;
import com.itlabil.tambalban.network.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataActivity extends AppCompatActivity {

    Button buttonSimpanData;
    EditText etDataNama,etDataBuka,etDataAlamat,etDataKendaraan,etDataLat,etDataLong;
    String pilihan;

    ApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        buttonSimpanData = (Button) findViewById(R.id.btnSimpanDataTBL);
        etDataNama = (EditText) findViewById(R.id.inputDataNama);
        etDataBuka = (EditText) findViewById(R.id.inputDataBuka);
        etDataAlamat = (EditText) findViewById(R.id.inputDataAlamat);
//        etDataKendaraan = (EditText) findViewById(R.id.inputDataKendaraan);
        etDataLat = (EditText) findViewById(R.id.inputDataLat);
        etDataLong = (EditText) findViewById(R.id.inputDataLong);

        etDataNama.setText("Tambal Ban");

        mApiService = ApiClient.getClient().create(ApiService.class);

        buttonSimpanData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<TambalBanModel> postTambalBan = mApiService.tambah(
                        etDataNama.getText().toString(),
                        etDataBuka.getText().toString(),
                        pilihan,
                        etDataAlamat.getText().toString(),
                        etDataLat.getText().toString(),
                        etDataLong.getText().toString()
                );
                postTambalBan.enqueue(new Callback<TambalBanModel>() {
                    @Override
                    public void onResponse(Call<TambalBanModel> call, Response<TambalBanModel> response) {
                        Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onFailure(Call<TambalBanModel> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                });

                etDataNama.setText("Tambal Ban");
                etDataBuka.setText(null);
                etDataAlamat.setText(null);
                etDataLat.setText(null);
                etDataLong.setText(null);
            }
        });
    }

    public void pilihKendaraan(View view) {
        boolean pilih = ((RadioButton) view).isChecked();
        switch (view.getId()){
            case R.id.pilihMotor:
                if (pilih)
                    pilihan = "Motor";
                break;
            case R.id.pilihMobil:
                if (pilih)
                    pilihan = "Mobil";
                break;
            case R.id.pilihMotorMobil:
                if (pilih)
                    pilihan = "Motor - Mobil";
                break;
        }
    }
}
