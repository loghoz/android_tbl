package com.itlabil.tambalban.pengaturan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.itlabil.tambalban.R;

public class FAQViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqview);

        Intent intent = getIntent();

        String textTitle = intent.getStringExtra("faqTitle");
        String textContent = intent.getStringExtra("faqContent");

        TextView faqTitle   = (TextView) findViewById(R.id.faqViewTitle);
        TextView faqContent = (TextView) findViewById(R.id.faqViewContent);

        faqTitle.setText(textTitle);
        faqContent.setText(textContent);
    }
}
