package com.itlabil.tambalban.menuutama;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.itlabil.tambalban.R;
import com.itlabil.tambalban.config.PengaturanListView;
import com.itlabil.tambalban.pengaturan.FaqActivity;
import com.itlabil.tambalban.pengaturan.TentangKamiActivity;

public class PengaturanActivity extends AppCompatActivity {

    ListView pengaturanList;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        String pengaturan_list[] = getResources().getStringArray(R.array.list_pengaturan);

        int pengaturan_list_icon[] = {
                R.drawable.ic_versi,
                R.drawable.ic_bantuan,
                R.drawable.ic_beri_nilai,
                R.drawable.ic_fp_facebook,
                R.drawable.ic_share_app,
                R.drawable.ic_tentang_kami
        };

        pengaturanList = (ListView)findViewById(R.id.pengaturan_list_view);
        PengaturanListView pengaturanAdapter = new PengaturanListView(getApplicationContext(), pengaturan_list, pengaturan_list_icon);
        pengaturanList.setAdapter(pengaturanAdapter);

        pengaturanList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent newActivity;
                String berbagiTitle = getString(R.string.berbagi_title);
                String berbagiSubject = getString(R.string.berbagi_subject);
                String berbagiText = getString(R.string.berbagi_text);
                String beriNilai = getString(R.string.beri_nilai_app);
                String fanPageFB = getString(R.string.fan_page_fb);

                switch( position )
                {
                    case 0:
                        showDialog();
                        break;

                    case 1:
                        newActivity = new Intent(PengaturanActivity.this, FaqActivity.class);
                        startActivity(newActivity);
                        break;

                    case 2:
                        newActivity = new Intent(android.content.Intent.ACTION_VIEW);
                        //Copy url App dari Google Play Store.
                        newActivity.setData(Uri.parse(beriNilai));
                        startActivity(newActivity);
                        break;

                    case 3:
                        newActivity = new Intent(android.content.Intent.ACTION_VIEW);
                        //Copy url App dari Facebook Fanpage.
                        newActivity.setData(Uri.parse(fanPageFB));
                        startActivity(newActivity);
                        break;

                    case 4:
                        newActivity = new Intent(android.content.Intent.ACTION_SEND);
                        newActivity.setType("text/plain");
                        newActivity.putExtra(android.content.Intent.EXTRA_SUBJECT, berbagiSubject);
                        newActivity.putExtra(android.content.Intent.EXTRA_TEXT, berbagiText);
                        startActivity(Intent.createChooser(newActivity, berbagiTitle));
                        break;

                    case 5:
                        newActivity = new Intent(PengaturanActivity.this, TentangKamiActivity.class);
                        startActivity(newActivity);
                        break;
                }

            }

        });
    }

    private void showDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_dialog_versi);
        dialog.show();
    }
}
