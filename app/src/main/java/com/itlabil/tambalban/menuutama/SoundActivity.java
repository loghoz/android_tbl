package com.itlabil.tambalban.menuutama;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.itlabil.tambalban.R;

public class SoundActivity extends AppCompatActivity {

    private SoundPool soundPool;

    private AudioManager audioManager;

    // Max sound stream
    private static final int MAX_STREAMS = 5;

    // Stream type.
    private static final int streamType = AudioManager.STREAM_MUSIC;

    private boolean loaded;

    private int angklungDo1,angklungRe1,angklungMi1,angklungFa1,angklungSo1,angklungLa1,angklungSi1,
            perkusi_1,perkusi_2,perkusi_3,angklungDo2,angklungDo3,angklungRe2,angklungMi2,angklungFa2,angklungSo2,angklungLa2,angklungSi2;
    private float volume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float currentVolumeIndex = (float) audioManager.getStreamVolume(streamType);
        float maxVolumeIndex  = (float) audioManager.getStreamMaxVolume(streamType);

        // Volumn (0 --> 1)
        this.volume = currentVolumeIndex / maxVolumeIndex;
        this.setVolumeControlStream(streamType);

        // Untuk Android SDK >= 21
        if (Build.VERSION.SDK_INT >= 21 ) {

            AudioAttributes audioAttrib = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            SoundPool.Builder builder= new SoundPool.Builder();
            builder.setAudioAttributes(audioAttrib).setMaxStreams(MAX_STREAMS);

            this.soundPool = builder.build();
        }
        // untuk Android SDK < 21
        else {
            this.soundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
        }

        // Ketika Sound Pool Selesai di Load.
        this.soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loaded = true;
            }
        });

        // Library Sound
        this.angklungDo1 = this.soundPool.load(this, R.raw.angklung_do_1,1);
        this.angklungDo2 = this.soundPool.load(this, R.raw.angklung_do_2,1);
        this.angklungDo3 = this.soundPool.load(this, R.raw.angklung_do_3,1);
        this.angklungRe1 = this.soundPool.load(this, R.raw.angklung_re_1,1);
        this.angklungRe2 = this.soundPool.load(this, R.raw.angklung_re_2,1);
        this.angklungMi1 = this.soundPool.load(this, R.raw.angklung_mi_1,1);
        this.angklungMi2 = this.soundPool.load(this, R.raw.angklung_mi_2,1);
        this.angklungFa1 = this.soundPool.load(this, R.raw.angklung_fa_1,1);
        this.angklungFa2 = this.soundPool.load(this, R.raw.angklung_fa_2,1);
        this.angklungSo1 = this.soundPool.load(this, R.raw.angklung_so_1,1);
        this.angklungSo2 = this.soundPool.load(this, R.raw.angklung_so_2,1);
        this.angklungLa1 = this.soundPool.load(this, R.raw.angklung_la_1,1);
        this.angklungLa2 = this.soundPool.load(this, R.raw.angklung_la_2,1);
        this.angklungSi1 = this.soundPool.load(this, R.raw.angklung_si_1,1);
        this.angklungSi2 = this.soundPool.load(this, R.raw.angklung_si_2,1);
        this.perkusi_1   = this.soundPool.load(this, R.raw.perkusi_1,1);
        this.perkusi_2   = this.soundPool.load(this, R.raw.perkusi_2,1);
        this.perkusi_3   = this.soundPool.load(this, R.raw.perkusi_3,1);
    }


    public void angklung_do1(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungDo1 = this.soundPool.play(this.angklungDo1,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_re1(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungRe1 = this.soundPool.play(this.angklungRe1,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_mi1(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungMi1 = this.soundPool.play(this.angklungMi1,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_fa1(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungFa1 = this.soundPool.play(this.angklungFa1,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_so1(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungSo1 = this.soundPool.play(this.angklungSo1,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_la1(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungLa1 = this.soundPool.play(this.angklungLa1,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_si1(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungSi1 = this.soundPool.play(this.angklungSi1,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_do2(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungDo2 = this.soundPool.play(this.angklungDo2,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_re2(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungRe2 = this.soundPool.play(this.angklungRe2,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_mi2(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungMi2 = this.soundPool.play(this.angklungMi2,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_fa2(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungFa2 = this.soundPool.play(this.angklungFa2,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_so2(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungSo2 = this.soundPool.play(this.angklungSo2,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_la2(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungLa2 = this.soundPool.play(this.angklungLa2,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_si2(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungSi2 = this.soundPool.play(this.angklungSi2,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void angklung_do3(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int angklungDo3 = this.soundPool.play(this.angklungDo3,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void perkusi_1(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int tambal = this.soundPool.play(this.perkusi_1,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void perkusi_2(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int ban = this.soundPool.play(this.perkusi_2,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void perkusi_3(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int lampung = this.soundPool.play(this.perkusi_3,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }
}
