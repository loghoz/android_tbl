package com.itlabil.tambalban.config;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.itlabil.tambalban.R;

/**
 * Created by itlabil on 22/11/17.
 */

public class PengaturanListView  extends BaseAdapter {
    Context context;
    String textPengaturan[];
    int iconPengaturan[];
    LayoutInflater inflter;

    public PengaturanListView(Context applicationContext, String[] textPengaturan, int[] iconPengaturan) {
        this.context = context;
        this.textPengaturan = textPengaturan;
        this.iconPengaturan = iconPengaturan;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return textPengaturan.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.activity_pengaturan_list_view, null);
        TextView text = (TextView) view.findViewById(R.id.textViewPengaturan);
        ImageView icon = (ImageView) view.findViewById(R.id.iconViewPengaturan);
        text.setText(textPengaturan[i]);
        icon.setImageResource(iconPengaturan[i]);
        return view;
    }

}
