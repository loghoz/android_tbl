package com.itlabil.tambalban.config;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by itlabil on 22/11/17.
 */

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // mode shared
    int PRIVATE_MODE = 0;

    // Nama Shared Pref
    private static final String PREF_NAME = "tambalbanlampung";

    private static final String IS_FIRST_TIME_LAUNCH = "PertamaBuka";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setPertamaBuka(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean pertamaBuka() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}
